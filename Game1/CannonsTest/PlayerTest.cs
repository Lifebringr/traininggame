﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cannons;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace CannonsTest
{
    [TestClass]
    public class PlayerTest
    {
        public MockedGraphicsDeviceService testingGraphicsService = new MockedGraphicsDeviceService();

        [TestMethod()]
        public void aPlayerGetsCreatedWithTheRightValues()
        {
            testingGraphicsService.CreateDevice();
            GraphicsDevice device = testingGraphicsService.GraphicsDevice;
            Player.TEXTURE_CARRIAGE = new Texture2D(device, 40, 40);
            Player.TEXTURE_CANNON = new Texture2D(device, 50, 50);
            Player unit = new Player("testId" , Color.White, new Vector2(50, 50));
            string id = string.Empty;
            Assert.AreEqual("testId", unit.id);
            Assert.AreEqual(Color.White,unit.color);
            Assert.AreEqual(50, unit.position.X);
            Assert.AreEqual(50, unit.position.Y);
            Assert.AreEqual(1F, unit.scaling);
            Assert.IsNotNull(unit.cannonColorArray);
            Assert.IsNotNull(unit.carriageColorArray);
        }
    }
}
