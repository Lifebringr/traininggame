﻿using Cannons;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CannonsTest
{
    
    
    /// <summary>
    ///This is a test class for GamePropertiesTest and is intended
    ///to contain all GamePropertiesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GamePropertiesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod()]
        public void gamePropertiesGetsCreatedWithTheRightDefaultValues()
        {
            GameProperties unit = new GameProperties();
            Assert.AreEqual(unit.numberOfPlayers, 4);
            Assert.AreEqual(unit.isScreenResolutionDependant, false);
            Assert.AreEqual(unit.isFullScreen, true);
            Assert.AreEqual(unit.preferredScreenWidth, 1024);
            Assert.AreEqual(unit.preferredScreenHeight, 728);
            Assert.AreEqual(unit.forceResolution, false);
            Assert.AreEqual(unit.title, "Cannons");

        }
    }
}
