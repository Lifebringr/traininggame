using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Cannons
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        GraphicsDevice device;
        SpriteBatch spriteBatch;

        KeyboardProcessor keyboardProcessor = new KeyboardProcessor();

        Texture2D backgroundTexture;
        Texture2D foregroundTexture;
        Texture2D groundTexture;
        Texture2D rocketTexture;
        Texture2D smokeTexture;
        Texture2D explosionTexture;

        SoundEffect hitCannon;
        SoundEffect hitTerrain;

        Vector2 baseScreenSize = new Vector2(800, 600);

        List<Particle> particleList = new List<Particle>();

        Color[,] rocketColorArray;
        Color[,] foregroundColorArray;

        Color[,] explosionColorArray;

        SpriteFont basicFont;

        Rocket rocket = new Rocket();
        List<Vector2> smokeList = new List<Vector2>();
        Random randomizer = new Random();

        int screenWidth;
        int screenHeight;
        Player[] players;
        GameProperties gameProperties = new GameProperties();
        int currentPlayer = 0;
        int[] terrainContour;



        protected int getScreenHeight()
        {
            return screenHeight;
        }
        protected int getScreenWidth()
        {
            return screenWidth;
        }
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            device = graphics.GraphicsDevice;

            backgroundTexture = Content.Load<Texture2D>("background");
            groundTexture = Content.Load<Texture2D>("ground");

            rocketTexture = Content.Load<Texture2D>("rocket");
            smokeTexture = Content.Load<Texture2D>("smoke");
            explosionTexture = Content.Load<Texture2D>("explosion");
            rocketColorArray = TextureUtils.textureTo2DArray(rocketTexture);
            explosionColorArray = TextureUtils.textureTo2DArray(explosionTexture);

            basicFont = Content.Load<SpriteFont>("basicFont");

            hitCannon = Content.Load<SoundEffect>("cannonhit");
            hitTerrain = Content.Load<SoundEffect>("terrainhit");
            Rocket.launchSound = Content.Load<SoundEffect>("launch").CreateInstance();

            if (gameProperties.forceResolution) {
                graphics.PreferredBackBufferWidth = gameProperties.preferredScreenWidth;
                graphics.PreferredBackBufferHeight = gameProperties.preferredScreenHeight;
            }
            
            graphics.IsFullScreen = gameProperties.isFullScreen;
            graphics.ApplyChanges();
            Window.Title = gameProperties.title;
            
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            if (gameProperties.isScreenResolutionDependant)
            {
                screenWidth = (int)baseScreenSize.X;
                screenHeight = (int)baseScreenSize.Y;
            }
            else
            {
                screenWidth = device.PresentationParameters.BackBufferWidth;
                screenHeight = device.PresentationParameters.BackBufferHeight;
            }

            GenerateRandomTerrainContour();
            SetUpPlayers();
            FlattenTerrainBelowPlayers();
            CreateForeground();
            
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (!rocket.isFlying && particleList.Count == 0)
            {
                keyboardProcessor.ProcessKeyboard(players[currentPlayer], rocket);
            }
            if (rocket.isFlying) {
                UpdateRocket();
                CheckCollisions(gameTime);
            }
            
            if (particleList.Count > 0)
            {
                UpdateParticles(gameTime);
            }


            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        private void UpdateRocket()
        {
                if (rocket.position.Y > this.screenHeight)
                {

                    nextPlayer();
                }
                Vector2 gravity = new Vector2(0, 1);
                rocket.direction += gravity / 10.0f;
                rocket.angle = (float)Math.Atan2(rocket.direction.X, -rocket.direction.Y);
                rocket.position += rocket.direction;

                for (int i = 0; i < 5; i++)
                {
                    Vector2 smokePos = rocket.position;
                    smokePos.X += randomizer.Next(10) - 5;
                    smokePos.Y += randomizer.Next(10) - 5;
                    smokeList.Add(smokePos);
                }
        }

        private void nextPlayer()
        {
            rocket.stop();
            smokeList.Clear();
            currentPlayer = currentPlayer + 1;
            currentPlayer = currentPlayer % gameProperties.numberOfPlayers;
            while (!players[currentPlayer].isAlive)
            {
                currentPlayer = ++currentPlayer % gameProperties.numberOfPlayers;
            }

        }

        

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Matrix globalTransformation = Matrix.CreateScale(CalculateScreenScalingFactor());

            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, globalTransformation);
            DrawScenery(spriteBatch);
            DrawPlayers(spriteBatch);
            DrawText(spriteBatch);
            DrawRocket(spriteBatch);
            DrawSmoke(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, globalTransformation);
            DrawExplosion();
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        private Vector3 CalculateScreenScalingFactor()
        {
            Vector3 screenScalingFactor;
            if (gameProperties.isScreenResolutionDependant)
            {
                float horScaling = (float)device.PresentationParameters.BackBufferWidth / baseScreenSize.X;
                float verScaling = (float)device.PresentationParameters.BackBufferHeight / baseScreenSize.Y;
                screenScalingFactor = new Vector3(horScaling, verScaling, 1);
            }
            else
            {
                screenScalingFactor = new Vector3(1, 1, 1);
            }
            return screenScalingFactor;
        }

        private void DrawRocket(SpriteBatch spriteBatch)
        {
            if (rocket.isFlying)
                spriteBatch.Draw(rocketTexture, rocket.position, null, players[currentPlayer].color, rocket.angle, new Vector2(42, 240), 0.1f, SpriteEffects.None, 1);
        }
        private void DrawScenery(SpriteBatch spriteBatch)
        {
            Rectangle screenRectangle = new Rectangle(0, 0, this.screenWidth, this.screenHeight);
            spriteBatch.Draw(backgroundTexture, screenRectangle, Color.White);
            spriteBatch.Draw(foregroundTexture, screenRectangle, Color.White);

        }

        private void DrawPlayers(SpriteBatch spriteBatch)
        {
            foreach (Player player in players)
            {
                player.draw(spriteBatch);
            }
        }

        private void DrawText(SpriteBatch spriteBatch)
        {
            Player activePlayer = players[currentPlayer];
            spriteBatch.DrawString(basicFont, "Power: " + activePlayer.power, new Vector2(20, 50), activePlayer.color);
            spriteBatch.DrawString(basicFont, "Angle: " + (int)MathHelper.ToDegrees(activePlayer.angle), new Vector2(20, 70), activePlayer.color);

        }

        private void DrawSmoke(SpriteBatch spriteBatch)
        {
            foreach (Vector2 smokePos in smokeList)
                spriteBatch.Draw(smokeTexture, smokePos, null, Color.White, 0, new Vector2(40, 35), 0.2f, SpriteEffects.None, 1);
        }
        private void SetUpPlayers()
        {
            Color[] playerColors = new Color[10];
            playerColors[0] = Color.Red;
            playerColors[1] = Color.Green;
            playerColors[2] = Color.Blue;
            playerColors[3] = Color.Purple;
            playerColors[4] = Color.Orange;
            playerColors[5] = Color.Indigo;
            playerColors[6] = Color.Yellow;
            playerColors[7] = Color.SaddleBrown;
            playerColors[8] = Color.Tomato;
            playerColors[9] = Color.Turquoise;

            players = new Player[gameProperties.numberOfPlayers];

            Player.TEXTURE_CANNON = Content.Load<Texture2D>("cannon");
            Player.TEXTURE_CARRIAGE = Content.Load<Texture2D>("carriage");

            for (int i = 0; i < gameProperties.numberOfPlayers; i++)
            {
                Vector2 playerPosition = new Vector2(screenWidth / (gameProperties.numberOfPlayers + 1) * (i + 1), terrainContour[screenWidth / (gameProperties.numberOfPlayers + 1) * (i + 1)]);
                players[i] = new Player(i.ToString(), playerColors[i], playerPosition);
            }

        }
        private void GenerateTerrainContour()
        {
            terrainContour = new int[screenWidth];

            for (int x = 0; x < screenWidth; x++)
                terrainContour[x] = screenHeight / 2;
        }

        private void GenerateRandomTerrainContour()
        {
            terrainContour = new int[screenWidth];

            double rand1 = randomizer.NextDouble() + 1;
            double rand2 = randomizer.NextDouble() + 2;
            double rand3 = randomizer.NextDouble() + 3;

            float offset = screenHeight / 2;
            float peakheight = 100;
            float flatness = 70;

            for (int x = 0; x < screenWidth; x++)
            {
                double height = peakheight / rand1 * Math.Sin((float)x / flatness * rand1 + rand1);
                height += peakheight / rand2 * Math.Sin((float)x / flatness * rand2 + rand2);
                height += peakheight / rand3 * Math.Sin((float)x / flatness * rand3 + rand3);
                height += offset;
                terrainContour[x] = (int)height;
            }
        }

        private void CreateForeground()
        {
            Color[,] groundColors = TextureUtils.textureTo2DArray(groundTexture);

            Color[] foregroundColors = new Color[screenWidth * screenHeight];

            for (int x = 0; x < screenWidth; x++)
            {
                for (int y = 0; y < screenHeight; y++)
                {
                    if (y > terrainContour[x])
                        foregroundColors[x + y * screenWidth] = groundColors[x % groundTexture.Width, y % groundTexture.Height];
                    else
                        foregroundColors[x + y * screenWidth] = Color.Transparent;
                }
            }
            foregroundTexture = new Texture2D(device, screenWidth, screenHeight, false, SurfaceFormat.Color);
            foregroundTexture.SetData(foregroundColors);
            foregroundColorArray = TextureUtils.textureTo2DArray(foregroundTexture);
        }
        private void FlattenTerrainBelowPlayers()
        {
            foreach (Player player in players)
            {
                if (player.isAlive)
                {
                    for (int x = 0; x < 40; x++)
                    {
                        terrainContour[(int)player.position.X + x] = terrainContour[(int)player.position.X];
                    }

                }

            }

        }


        private Vector2 TexturesCollide(Color[,] tex1, Matrix mat1, Color[,] tex2, Matrix mat2)
        {
            Matrix mat1to2 = mat1 * Matrix.Invert(mat2);

            int width1 = tex1.GetLength(0);
            int height1 = tex1.GetLength(1);
            int width2 = tex2.GetLength(0);
            int height2 = tex2.GetLength(1);

            for (int x1 = 0; x1 < width1; x1++)
            {

                for (int y1 = 0; y1 < height1; y1++)
                {
                    Vector2 pos1 = new Vector2(x1, y1);
                    Vector2 pos2 = Vector2.Transform(pos1, mat1to2);

                    int x2 = (int)pos2.X;
                    int y2 = (int)pos2.Y;
                    if ((x2 >= 0) && (x2 < width2))
                    {
                        if ((y2 >= 0) && (y2 < height2))
                        {
                            if (tex1[x1, y1].A > 0)
                            {
                                if (tex2[x2, y2].A > 0)
                                {
                                    Vector2 collisionPosition = Vector2.Transform(pos1, mat1);
                                    return collisionPosition;
                                }
                            }
                        }
                    }

                }
            }

            return new Vector2(-1, -1);
        }

        private Vector2 CheckTerrainCollision()
        {
            Matrix rocketMat = Matrix.CreateTranslation(-42, -240, 0) * Matrix.CreateRotationZ(rocket.angle) * Matrix.CreateScale(rocket.scaling) * Matrix.CreateTranslation(rocket.position.X, rocket.position.Y, 0);
            Matrix terrainMat = Matrix.Identity;
            Vector2 terrainCollisionPoint = TexturesCollide(rocketColorArray, rocketMat, foregroundColorArray, terrainMat);
            return terrainCollisionPoint;

        }

        private Vector2 CheckPlayersCollision()
        {
            Matrix rocketMat = Matrix.CreateTranslation(-42, -240, 0) * Matrix.CreateRotationZ(rocket.angle) * Matrix.CreateScale(rocket.scaling) * Matrix.CreateTranslation(rocket.position.X, rocket.position.Y, 0);
            for (int i = 0; i < gameProperties.numberOfPlayers; i++)
            {
                Player player = players[i];
                if (player.isAlive)
                {
                    if (i != currentPlayer)
                    {
                        int xPos = (int)player.position.X;
                        int yPos = (int)player.position.Y;

                        Matrix carriageMat = Matrix.CreateTranslation(0, -Player.TEXTURE_CARRIAGE.Height, 0) * Matrix.CreateScale(player.scaling) * Matrix.CreateTranslation(xPos, yPos, 0);
                        Vector2 carriageCollisionPoint = TexturesCollide(player.carriageColorArray, carriageMat, rocketColorArray, rocketMat);
                        if (carriageCollisionPoint.X > -1)
                        {
                            players[i].isAlive = false;
                            return carriageCollisionPoint;
                        }
                        Matrix cannonMat = Matrix.CreateTranslation(-11, -50, 0) * Matrix.CreateRotationZ(player.angle) * Matrix.CreateScale(player.scaling) * Matrix.CreateTranslation(xPos + 20, yPos - 10, 0);
                        Vector2 cannonCollisionPoint = TexturesCollide(player.cannonColorArray, cannonMat, rocketColorArray, rocketMat);
                        if (cannonCollisionPoint.X > -1)
                        {
                            players[i].isAlive = false;
                            return cannonCollisionPoint;
                        }
                    }
                }
            }
            return new Vector2(-1, -1);
        }

        private void CheckCollisions(GameTime gameTime)
        {
                Vector2 terrainCollisionPoint = CheckTerrainCollision();
                Vector2 playerCollisionPoint = CheckPlayersCollision();

                if (playerCollisionPoint.X > -1)
                {
                    AddExplosion(playerCollisionPoint, 10, 80.0f, 2000.0f, gameTime);
                    hitCannon.Play();
                    smokeList = new List<Vector2>();
                    nextPlayer();
                }
                else if (terrainCollisionPoint.X > -1)
                {
                    AddExplosion(terrainCollisionPoint, 4, 30.0f, 1000.0f, gameTime);

                    hitTerrain.Play();

                    smokeList = new List<Vector2>();
                    nextPlayer();
                }

        }

        private void AddExplosion(Vector2 explosionPos, int numberOfParticles, float size, float maxAge, GameTime gameTime)
        {
            for (int i = 0; i < numberOfParticles; i++)
                AddExplosionParticle(explosionPos, size, maxAge, gameTime);

            float rotation = (float)randomizer.Next(10);
            Matrix mat = Matrix.CreateTranslation(-explosionTexture.Width / 2, -explosionTexture.Height / 2, 0) * Matrix.CreateRotationZ(rotation) * Matrix.CreateScale(size / (float)explosionTexture.Width * 2.0f) * Matrix.CreateTranslation(explosionPos.X, explosionPos.Y, 0);
            AddCrater(explosionColorArray, mat);
            for (int i = 0; i < players.Length; i++)
                players[i].position.Y = terrainContour[(int)players[i].position.X];
            FlattenTerrainBelowPlayers();
            CreateForeground();

        }

        private void AddExplosionParticle(Vector2 explosionPos, float explosionSize, float maxAge, GameTime gameTime)
        {
            Particle particle = new Particle();

            particle.OrginalPosition = explosionPos;
            particle.Position = particle.OrginalPosition;

            particle.BirthTime = (float)gameTime.TotalGameTime.TotalMilliseconds;
            particle.MaxAge = maxAge;
            particle.Scaling = 0.25f;
            particle.ModColor = Color.White;

            float particleDistance = (float)randomizer.NextDouble() * explosionSize;
            Vector2 displacement = new Vector2(particleDistance, 0);
            float angle = MathHelper.ToRadians(randomizer.Next(360));
            displacement = Vector2.Transform(displacement, Matrix.CreateRotationZ(angle));

            particle.Direction = displacement * 2.0f;
            particle.Accelaration = -particle.Direction;

            particleList.Add(particle);
        }
        private void DrawExplosion()
        {
            for (int i = 0; i < particleList.Count; i++)
            {
                Particle particle = particleList[i];
                spriteBatch.Draw(explosionTexture, particle.Position, null, particle.ModColor, i, new Vector2(256, 256), particle.Scaling, SpriteEffects.None, 1);
            }
        }
        private void UpdateParticles(GameTime gameTime)
        {
            float now = (float)gameTime.TotalGameTime.TotalMilliseconds;
            for (int i = particleList.Count - 1; i >= 0; i--)
            {
                Particle particle = particleList[i];
                float timeAlive = now - particle.BirthTime;

                if (timeAlive > particle.MaxAge)
                {
                    particleList.RemoveAt(i);
                }
                else
                {
                    float relAge = timeAlive / particle.MaxAge;
                    particle.Position = 0.5f * particle.Accelaration * relAge * relAge + particle.Direction * relAge + particle.OrginalPosition;
                    float invAge = 1.0f - relAge;
                    particle.ModColor = new Color(new Vector4(invAge, invAge, invAge, invAge));
                    Vector2 positionFromCenter = particle.Position - particle.OrginalPosition;
                    float distance = positionFromCenter.Length();
                    particle.Scaling = (50.0f + distance) / 200.0f;
                    particleList[i] = particle;

                }
            }
        }

        private void AddCrater(Color[,] texture, Matrix explosionMatrix)
        {
            int width = texture.GetLength(0);
            int height = texture.GetLength(1);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (!isBlack(texture, x, y))
                    {
                        Vector2 imagePos = new Vector2(x, y);
                        Vector2 screenPos = Vector2.Transform(imagePos, explosionMatrix);

                        int screenX = (int)screenPos.X;
                        int screenY = (int)screenPos.Y;

                        if ((screenX) > 0 && (screenX < screenWidth))
                            if (terrainContour[screenX] < screenY)
                                terrainContour[screenX] = screenY;
                    }
                }
            }
        }

        private static bool isBlack(Color[,] texture, int x, int y)
        {
            return texture[x, y].R <= 10;
        }
    }


}
