﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Cannons
{
    public class Player
    {
        public bool isAlive = true;
        public float angle = MathHelper.ToRadians(45);
        public float power = 100;
        public Vector2 position;
        readonly public Color color;
        public readonly string id;
        public readonly float scaling;
        public Color[,] carriageColorArray;
        public Color[,] cannonColorArray;

        Vector2 cannonOrigin = new Vector2(11, 50);



        public Player(String id, Color color, Vector2 position)
        {
            this.id = id;
            this.color = color;
            this.position = position;
            this.scaling = 40.0f / (float)TEXTURE_CARRIAGE.Width;
            this.carriageColorArray = TextureUtils.textureTo2DArray(TEXTURE_CARRIAGE);
            this.cannonColorArray = TextureUtils.textureTo2DArray(TEXTURE_CANNON);

        }
        internal void draw(SpriteBatch spriteBatch)
        {
            if (this.isAlive)
            {
                spriteBatch.Draw(TEXTURE_CANNON, new Vector2(this.position.X + 20, this.position.Y - 10), null, this.color, angle, this.cannonOrigin, this.scaling, SpriteEffects.None, 1);
                spriteBatch.Draw(TEXTURE_CARRIAGE, this.position, null, this.color, 0, new Vector2(0, TEXTURE_CARRIAGE.Height), this.scaling, SpriteEffects.None, 0);
            }

        }

        public static Texture2D TEXTURE_CARRIAGE { get; set; }
        public static Texture2D TEXTURE_CANNON { get; set; }
    }
}
