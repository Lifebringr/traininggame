﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Cannons
{
    class KeyboardProcessor
    {
        public void ProcessKeyboard(Player currentPlayer, Rocket rocket)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Down) && currentPlayer.angle <= MathHelper.PiOver2)
            {
                currentPlayer.angle += 0.01F;
            }

            if (keyboardState.IsKeyDown(Keys.Up) && currentPlayer.angle >= -MathHelper.PiOver2)
            {
                currentPlayer.angle -= 0.01F;
            }

            if (keyboardState.IsKeyDown(Keys.PageDown) && currentPlayer.power > 0)
                currentPlayer.power -= 20;
            if (keyboardState.IsKeyDown(Keys.PageUp) && currentPlayer.power < 1000)
                currentPlayer.power += 20;

            if ((keyboardState.IsKeyDown(Keys.Enter) || keyboardState.IsKeyDown(Keys.Space)) && !rocket.isFlying)
            {
                rocket.fire();


                rocket.position = currentPlayer.position;
                rocket.position.X += 20;
                rocket.position.Y -= 10;
                rocket.angle = currentPlayer.angle;

                Vector2 up = new Vector2(0, -1);
                Matrix rotationMatrix = Matrix.CreateRotationZ(rocket.angle);
                rocket.direction = Vector2.Transform(up, rotationMatrix);
                rocket.direction *= currentPlayer.power / 50.0f;


            }

        }
    }
}
