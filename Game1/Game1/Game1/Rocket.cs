﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Cannons
{
    public class Rocket
    {
        public bool isFlying;
        public Vector2 position;
        public Vector2 direction;
        public float angle;
        public float scaling = 0.1F;
        public static SoundEffectInstance launchSound;

        internal void fire()
        {
            this.isFlying = true;

            launchSound.Play();
        }

        internal void stop()
        {
            this.isFlying = false;
            launchSound.Stop();
        }
    }
}
