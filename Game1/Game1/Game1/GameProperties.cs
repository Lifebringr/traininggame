﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cannons
{
    class GameProperties
    {
        public int numberOfPlayers = 4;
        public bool isScreenResolutionDependant = false;
        public int preferredScreenWidth = 1024;
        public int preferredScreenHeight = 728;
        public bool isFullScreen = true;
        public bool forceResolution = false;
        public string title = "Cannons";
    }
}
